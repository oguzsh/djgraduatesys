from django.conf.urls import url, include
from django.contrib import admin
from home.views import *

urlpatterns = [

    url(r'^$', home_view, name="main"),

    url(r'^admin/', admin.site.urls),

    url(r'^contact/', contact_view, name="contact"),

    url(r'^yonetici/', yonet_view, name="yonetici"),

    url(r'^mezun/', mezun_view, name="mezun"),


]

