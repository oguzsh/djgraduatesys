from django.shortcuts import render, HttpResponse


def home_view(request):
    if request.user.is_authenticated():
        context = {
            'isim': 'Oğuzhan'
        }
    else:
        context = {
            'isim': 'Misafir Kullanıcı'
        }
    return render(request, 'home.html', context)

def contact_view(request):
    return render(request, 'contact.html')

def yonet_view(request):
    return render(request, 'yonetim/index.html')

def mezun_view(request):
    return render(request, 'mezun/index.html')