﻿# Django Graduate System

### Project Setup

**:black_circle: Create a Virtual Environment in the Project Directory**

```bash
 virtualenv env
```

**:black_circle: Activate the Virtual Environment**

* Linux:
```bash
  source bin/activate
```

* Windows:
```bash
  env\Scripts\activate
```

**:black_circle: Do the installation of Django 1.10**
```bash
  pip install django==1.10
```

**:black_circle: Do JQuery setup**
```bash
  pip install django-jquery
```

**:black_circle: Migrate the project**
```bash
  python manage.py migrate
```

**:black_circle: Create a superuser (admin) account in the project**
```bash
  python manage.py createsuperuser
  
  **Output**
  
  Username (leave blank to use 'pc'): **Your Nickname**
  Email adress: **Your Email adress**
  Password: **Your password**
  Password (again): **Repeat again**
  
  **Superuser created successfully.**
```


***:black_circle: Run the project emoji! :white_check_mark:**
```bash
  python manage.py runserver
  
  **Django works by default as port 8000 if you want to use another port;**
  
  python manage.py runserver [the_port_number_you_want]
  
  **Example**
  python manage.py runserver 8888
```



